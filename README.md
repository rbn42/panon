![](../../wiki/screenshot.png)

If you are interested in `panon`, please let me know, so I will invest more time in it. Bug reports, feature requests and suggestions are welcome.

[See previews](../../wiki/Previews).

Installation
===========
`PKBGUILD` file is provided for Arch Linux. Alternatively, You may install `panon` from pypi.
```
pip install panon --user
```
But make sure you have `wmctrl` installed for window control and `pamixer` installed for volume control.

Credits
======
Some code parts of taskbar section and visualizer section are adapted from [PyPanel](http://pypanel.sourceforge.net/) and [PyVisualizer](https://github.com/ajalt/PyVisualizer).

The taskbar section is also inspired by [Docky](http://www.go-docky.com/). Multiload section is inspired by [gnome multiload panel applet](https://github.com/paradoxxxzero/gnome-shell-system-monitor-applet) and shortcuts section by [yabar](https://github.com/geommer/yabar).

